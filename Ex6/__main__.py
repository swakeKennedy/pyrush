#!/usr/bin/python3
import sys
if (len(sys.argv) < 4):
    print("Not enough arguments")
else:
    try:
        fd_1 = open(sys.argv[2],"r")
        fd_1.close()
    except:
        print ("Can't open conf file")
        exit()
    else:
        try:
            fd_2 = open(sys.argv[3],"r")
            fd_2.close()
        except:
            print ("Can't open the file to distort")
            exit()
        else:
            conf = open(sys.argv[2], "r")
            conf_l = conf.readlines()
            mod = open(sys.argv[3], "r")
            d = {}
            i = 0;
            while (i < len(conf_l)):
                if (sys.argv[1] == "cipher" and len(conf_l[i]) >= 3):
                    d[conf_l[i][0]] = conf_l[i][2]
                elif (sys.argv[1] == "decipher" and len(conf_l[i]) >= 3):
                    d[conf_l[i][2]] = conf_l[i][0]
                elif (len(conf_l[i]) >= 3):
                    print ("Command not recognized")
                    exit()
                i = i + 1
            mod_r = mod.read()
            i = 0
            new = ""
            while (i < len(mod_r)):
                try:
                    d[mod_r[i]]
                except:
                    new += mod_r[i]
                else:
                    new += d[mod_r[i]]
                i = i + 1
            print(new)
            conf.close()
            mod.close()

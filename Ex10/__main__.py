#!/usr/bin/python3
import sys
if (len(sys.argv) < 2):
    print("Not enough arguments")
    exit()
i = 1
val = (int) (sys.argv[1])
if (val == 0):
    print("0")
while (i <= val):
    result = list(range(i, val * i + 1, i))
    print (*result, sep=' ')
    i = i + 1

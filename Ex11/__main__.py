#!/usr/bin/python3
import sys
if (len(sys.argv) < 2):
    print("Not enough arguments")
    exit()
if (len(sys.argv) == 3 and sys.argv[2] == "-d"):
    i = 0
    if (sys.argv[1] == "num"):
        while (i < 10):
            print("'", i,"'"," (",i + 48,")", sep='')
            i = i + 1
    if (sys.argv[1] == "alpha"):
        while (i < 26):
            print("'", chr(i + 97),"'"," (",i + 97,")", sep='')
            i = i + 1
    if (sys.argv[1] == "alphanum"):
        while (i < 26):
            print("'", chr(i + 97),"'"," (",i + 97,")", sep='')
            i = i + 1
        i = 0
        while (i < 10):
            print("'", i,"'"," (",i + 48,")", sep='')
            i = i + 1
else:
    i = 0
    if (sys.argv[1] == "num"):
        while (i < 10):
            print("'", i,"'"," (",hex(i + 48),")", sep='')
            i = i + 1
    if (sys.argv[1] == "alpha"):
        while (i < 26):
            print("'", chr(i + 97),"'"," (",hex(i + 97),")", sep='')
            i = i + 1
    if (sys.argv[1] == "alphanum"):
        while (i < 26):
            print("'", chr(i + 97),"'"," (",hex(i + 97),")", sep='')
            i = i + 1
        i = 0
        while (i < 10):
            print("'", i,"'"," (",hex(i + 48),")", sep='')
            i = i + 1

#!/usr/bin/python3
import sys
del sys.argv[0]
sys.argv = sorted(sys.argv, key=int)
if (len(sys.argv) > 0):
        print(" <= ".join(list(sys.argv)))
else:
        print("No arguments")

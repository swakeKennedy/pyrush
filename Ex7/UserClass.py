#!/usr/bin/python3

class User:
    def __init__(self, login, password, promo):
        self.login = login
        self.password = password
        self.promo = promo
        self.my_list = []
    def __repr__(self):
        a = "<User '"
        a += self.login
        a += "'>"
        return (a)
    def add_binome(self, usr2):
        x = 0
        while (x < len(self.my_list) and self.my_list[x] != usr2.login):
            x = x + 1
        if (x == len(self.my_list)):
            self.my_list.append(usr2.login)
            usr2.my_list.append(self.login)
        else:
            a = usr2.login
            a += " is already in the list"
            print(a)
    def print_binomes(self):
        if (len(self.my_list) == 1):
            a = "Le binome de "
            a += self.login
            a += " est : "
            a += self.my_list[0]
            print (a)
        if (len(self.my_list) > 1):
            a = "Les binomes de "
            a += self.login
            a += " sont : "
            a += ", ".join(self.my_list)
            print (a)
